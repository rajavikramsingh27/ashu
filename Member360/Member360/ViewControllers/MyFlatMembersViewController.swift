//  MyFlatMembersViewController.swift
//  Member360

//  Created by Boons&Blessings Of Codes Pvt Ltd on 20/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.


import UIKit


class MyFlatMembersViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
}

extension MyFlatMembersViewController:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for:indexPath)
        
        return cell
    }
    
}
