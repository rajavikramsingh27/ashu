//
//  LoginViewController.swift
//  Member360
//
//  Created by BlessingsOfCodes Pvt Ltd on 15/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnGenerateOTP(_ sender:UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"OTPViewController") as! OTPViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }

}
