//
//  ViewController.swift
//  Member360
//
//  Created by BlessingsOfCodes Pvt Ltd on 15/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline:.now()+3) {
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated:true)
        }
    }


}

