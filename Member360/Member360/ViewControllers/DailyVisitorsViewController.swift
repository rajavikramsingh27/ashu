//
//  DailyVisitorsViewController.swift
//  Member360
//
//  Created by Boons&Blessings Of Codes Pvt Ltd on 20/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class DailyVisitorsViewController: UIViewController {
    @IBOutlet weak var tblDailyVisitors:UITableView!
    
    var isStaff = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func btnStaffResident(_ sender:UIButton) {
        if sender.tag == 0 {
            isStaff = false
        } else {
            isStaff = true
        }
        tblDailyVisitors.reloadData()
    }
    
    
}



extension DailyVisitorsViewController:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isStaff {
            let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for:indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier:"cellResident", for:indexPath)
            
            return cell
        }
    }
    
}
