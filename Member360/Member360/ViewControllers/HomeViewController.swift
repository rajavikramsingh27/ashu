
//
//  HomeViewController.swift
//  Member360
//
//  Created by BlessingsOfCodes Pvt Ltd on 15/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var coll:UICollectionView!
    
    let arrTitles = ["Visitors","Preauthorize Visitors","Daily Visitor","Add Flat Members","Emergecny Alert","Notice Board","Admin Panel","Member Chats","Send Complain","Complain Box"]
    
    let arrIcons = ["contact.png","contact (1).png","pilot.png","follow.png","caution.png","teacher.png","admin.png","chat.png","alert.png","complaint.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnProfile(_ sender:UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }


}


extension HomeViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2-30
        print("width is:-",width)
        return CGSize (width:width, height:width)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for:indexPath) as! HomeCollectionViewCell
        
        cell.lblTitle.text = arrTitles[indexPath.row]
        cell.imgIcons.image = UIImage (named:arrIcons[indexPath.row])
        
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action:#selector(btnSelect(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @IBAction func btnSelect(_ sender:UIButton) {
        if sender.tag == 1 {
            pushToVC("AddVisitorsViewController")
        } else if sender.tag == 2 {
            pushToVC("DailyVisitorsViewController")
        } else if sender.tag == 3 {
            pushToVC("MyFlatMembersViewController")
        } else if sender.tag == 4 {
            pushToVC("EmergencyAlertViewController")
        } else if sender.tag == 5 {
            pushToVC("NoticeBoardViewController")
        } else if sender.tag == 7 {
            pushToVC("MebmberChatViewController")
        } else if sender.tag == 8 {
            pushToVC("ComplaintAddViewController")
        } else if sender.tag == 9 {
            pushToVC("ComplaintListViewController")
        }
    }
    
    
}



class HomeCollectionViewCell:UICollectionViewCell {
    @IBOutlet weak var viewContainer:UIView!
    @IBOutlet weak var imgIcons:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var btnSelect:UIButton!
    
    
    
    override func awakeFromNib() {
        
    }
}



extension UIViewController {
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func pushToVC(_ identifier:String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:identifier)
        self.navigationController?.pushViewController(vc!, animated:true)
    }

}
