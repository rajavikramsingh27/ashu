//
//  MebmberChatViewController.swift
//  Member360
//
//  Created by Boons&Blessings Of Codes Pvt Ltd on 21/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class MebmberChatViewController: UIViewController {
    @IBOutlet weak var viewSearch:UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewSearch.isHidden = true
    }
    
    @IBAction func btnSubmit(_ sender:UIButton) {
        viewSearch.isHidden = false
    }
    
    @IBAction func btnCancelSearch(_ sender:UIButton) {
        viewSearch.isHidden = true
    }

}


extension MebmberChatViewController:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for:indexPath)
        
        return cell
    }
    
}
