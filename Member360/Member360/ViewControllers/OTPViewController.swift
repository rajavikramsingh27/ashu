//
//  OTPViewController.swift
//  Member360
//
//  Created by BlessingsOfCodes Pvt Ltd on 15/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit
import OTPFieldView

class OTPViewController: UIViewController,OTPFieldViewDelegate {

    @IBOutlet weak var textOTP: OTPFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textOTP.fieldsCount = 6
        textOTP.fieldBorderWidth = 1
        textOTP.defaultBorderColor = UIColor.darkGray
        textOTP.filledBorderColor = UIColor.darkGray
        textOTP.cursorColor = UIColor.blue
        textOTP.displayType = .roundedCorner
        textOTP.fieldSize = 44
        textOTP.separatorSpace = 10
        textOTP.shouldAllowIntermediateEditing = false
        textOTP.delegate = self
        textOTP.initializeUI()
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
            
        return true
    }
    
    func enteredOTP(otp: String) {
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return true
    }
    
    @IBAction func btnSubmit(_ sender:UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(vc, animated:true)
    }

    
}
