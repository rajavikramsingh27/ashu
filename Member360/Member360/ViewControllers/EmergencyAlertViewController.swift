//
//  EmergencyAlertViewController.swift
//  Member360
//
//  Created by Boons&Blessings Of Codes Pvt Ltd on 20/July/2020.
//  Copyright © 2020 BlessingsOfCodes Pvt Ltd. All rights reserved.
//

import UIKit

class EmergencyAlertViewController: UIViewController {

    @IBOutlet weak var coll:UICollectionView!
    
    let arrTitles = ["Fire Alert","Lift Alert","Theft Alert","Other Alert"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}


extension EmergencyAlertViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2-30
        print("width is:-",width)
        return CGSize (width:width, height:140)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for:indexPath) as! HomeCollectionViewCell
        
        cell.lblTitle.text = arrTitles[indexPath.row]
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action:#selector(btnSelect(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @IBAction func btnSelect(_ sender:UIButton) {
//        if sender.tag == 1 {
            pushToVC("AddEmergencyAlertViewController")
//        } else if sender.tag == 2 {
//            pushToVC("DailyVisitorsViewController")
//        } else if sender.tag == 4 {
//            pushToVC("MyFlatMembersViewController")
//        } else if sender.tag == 5 {
//            pushToVC("EmergencyAlertViewController")
//        }
    }
        
}
